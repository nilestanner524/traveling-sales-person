﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    class PriorityQueue
    {

        private List<matrix> queue = new List<matrix>();
        int maxSize = 0;

        //insert is O(n)
        public void insert(matrix node)
        {
            queue.Add(node);
            if(maxSize < queue.Count)
            {
                maxSize = queue.Count;
            }
        }

        public matrix deleteMin()
        {
            matrix min = new matrix();
            min.lowerBound = double.PositiveInfinity;
            int minIndex = -1;
            //search through the entire array O(n) where n is the size of the queue
            for (int i = 0; i < queue.Count; i++)
            {
                //short calculation combining Lowerbound and depth
                if (queue[i].lowerBound - (queue[i].depth * 20) < min.lowerBound)
                {
                    min = queue[i];
                    minIndex = i;
                }
            }
            //if by some off chance we don find anything just return the first one
            if (minIndex == -1)
            {
                minIndex = 0;
            }
            matrix returnValue = queue[minIndex];
            queue.RemoveAt(minIndex);
            return returnValue;
        }

        public bool isEmpty()
        {
            //simple function to see if the queue is empty
            return queue.Count == 0;
        }

    }
}
