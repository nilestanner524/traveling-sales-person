﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TSP;

namespace WindowsFormsApplication1
{
    class matrix
    {
        //total space complexity is O(n^2) + O(n) (matrixData + path)
        private double[,] matrixData;
        public double lowerBound;
        public int[] path;
        public int current;
        public int depth;

        public void setUpMatrix(City[] cities)
        {
            //set up fresh matrix
            current = 0;
            depth = 1;
            path = new int[cities.Length];
            matrixData = new double[cities.Length, cities.Length];

            //we need to fill the matrix with the inital values
            //assuming that costToGetTo is constant this will still result in O(n^2) time complexity where n is the number of cities
            //space complexity is also O(n^2) becuase of the 2d array
            for (int i = 0; i < cities.Length; i++)
            {
                for (int j = 0; j < cities.Length; j++)
                {
                    matrixData[i, j] = cities[i].costToGetTo(cities[j]);

                    //if the value is 0 this is because it is going from it's self to it self, change to infinity 
                    if(matrixData[i, j] == 0)
                    {
                        matrixData[i, j] = double.PositiveInfinity;
                    }
                }
            }
            lowerBound = 0;
        }

        
        public void copyMatrix(int col, matrix m)
        {
            //not counting the cloning this algorith takes O(n) time;
            //col is the city index we are going to
            //m.current is the city index we are coming from
            //m is the state that we are branching from

            //adopt parents values
            lowerBound = m.lowerBound;
            matrixData = (double[,])m.matrixData.Clone();
            path = (int[])m.path.Clone();

            //add cost to this city to lower bound
            lowerBound += m.matrixData[m.current, col];

            //becuase we branched we are now deeper, or our route is longer
            depth = m.depth + 1;

            //path will allow us to keep track of where we have been (more on this below in getRoute())
            path[m.current] = col;

            //update current city
            current = col;
            
            //set row (m.current) and col (col) in matrixData to inifinity to show that that route is taken O(n) time complexity
            for(int k = 0; k < matrixData.GetLength(0); k++)
            {
                matrixData[m.current, k] = matrixData[k,col] = double.PositiveInfinity;
            }
        }

        public double reduceMatrix()
        {
            //in total this algorithm for reducing takes O(n(2n) + n(2n)) or O(n^2)
            double total = 0;
            //for each row, time complexity is O(n^2) as it has to go over the entire matrix
            for (int i = 0; i < matrixData.GetLength(0); i++)
            {
                double lowest = double.PositiveInfinity;
                for (int j = 0; j < matrixData.GetLength(0); j++)
                {
                    if (matrixData[i,j] == 0)
                    {
                        //if we hit a zero, theres no need to reduce
                        lowest = 0;
                        break;
                    }
                    if (matrixData[i, j] < lowest)
                    {
                        lowest = matrixData[i, j];
                    }
                }
                //if there is no zero in the row, not all the values are infinity we need to reduce the row
                if(lowest > 0 && !double.IsPositiveInfinity(lowest))
                {
                    reduceRow(lowest, i);
                    total += lowest;
                }
            }

            //for each column, time complexity is O(n^2) as it has to go over the entire matrix
            for (int j = 0; j < matrixData.GetLength(0); j++)
            {
                double lowest = double.PositiveInfinity;
                for (int i = 0; i < matrixData.GetLength(0); i++)
                {
                    if (matrixData[i, j] == 0)
                    {
                        //if we hit a zero, theres no need to reduce
                        lowest = 0;
                        break;
                    }
                    if (matrixData[i, j] < lowest)
                    {
                        lowest = matrixData[i, j];
                    }
                }
                //if there is no zero in the row, not all the values are infinity we need to reduce the col
                if (lowest > 0 && !double.IsPositiveInfinity(lowest))
                {
                    reduceCol(lowest, j);
                    total += lowest;
                }
                
               
            }
            //total amount reduced is saved to total, add it to the lowerbound
            lowerBound += total;
            return total;
        }

        private void reduceRow(double value, int i)
        {
            //decrement every element in the row, time complexity of O(n)
            for(int j = 0; j < matrixData.GetLength(0); j++)
            {
                matrixData[i, j] -= value;
            }
        }

        private void reduceCol(double value, int j)
        {
            //decrement every element in the col, time complexity of O(n)
            for (int i = 0; i < matrixData.GetLength(0); i++)
            {
                matrixData[i, j] -= value;
            }
        }

        public ArrayList getRoute(City[] cities)
        {
            ArrayList route = new ArrayList();
            //we assume that the path starts at index 0
            int next = 0;
            //time complexity of O(n)
            do
            {
                //the next city index will be the value at the previous city index
                next = path[next];
                // add the city to the route
                route.Add(cities[next]);
                //we exit when we hit zero because we know that it had made a full circle. 
            } while (next != 0);
                return route;
        }
    }
}
